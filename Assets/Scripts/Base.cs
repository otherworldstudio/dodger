﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Base : MonoBehaviour
    {
        public float LeftBoundary = 0;
        public float RightBoundary = 0;
        
        public float Speed = 1;
    }
}

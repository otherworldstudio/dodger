﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadNote : MovingUp {

    private bool Counted = false;
    private void LateUpdate()
    {
        if (transform.position.y > 2.5 && !Counted && !Controller.GameOver)
        {
            Counted = !Counted;
            Controller.BadNoteAvoided();
        }
    }
}

﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gentleman : Base {
    
    private float defaultHeight = 2f;

    private void Start()
    {
        transform.position = new Vector2((LeftBoundary + RightBoundary)/2, defaultHeight);
    }

    private void Update()
    {
        if (!Controller.GameOver)
        {
            if (Input.GetKey(KeyCode.LeftArrow) && transform.position.x > LeftBoundary)
            {
                transform.position = new Vector2(transform.position.x - Speed * Time.deltaTime, defaultHeight);
            }

            if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < RightBoundary)
            {
                transform.position = new Vector2(transform.position.x + Speed * Time.deltaTime, defaultHeight);
            }

            //transform.position = new Vector2(transform.position.x, transform.position.y + Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!Controller.GameOver)
        {
            if (collider.gameObject.tag == "Enemy")
            {
                Controller.GentlemanIsHit(collider.gameObject);
            }
            if (collider.gameObject.tag == "Note")
            {
                Controller.NoteCollected(collider.gameObject);
            }
        }
    }
}

﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {
    
    public Gentleman Gentleman;
    public GameObject BadNote;
    public GameObject Cloud;
    public GameObject Note;

    public GameObject EnemyContainer;
    public GameObject CloudContainer;
    public GameObject NoteContainer;

    public static bool GameOver = false;
    public GameObject GameOverPanel;
    public Text GameOverPanelText;

    public static int Points = 0;
    public Text PointsLabel;
    public static int Lives = 3;
    public Text LivesLabel;

    private float BadNoteSpawnRatio = 0;
    private float BadNoteLeftBoundary = 0;
    private float BadNoteRightBoundary = 0;

    private float CloudSpawnRatio = 0;
    private float CloudLeftBoundary = 0;
    private float CloudRightBoundary = 0;

    private float NoteSpawnRatio = 0;
    private float NoteLeftBoundary = 0;
    private float NoteRightBoundary = 0;

    private void Start()
    {
        BadNoteSpawnRatio = BadNote.GetComponent<BadNote>().SpawnRatio;
        BadNoteLeftBoundary = BadNote.GetComponent<BadNote>().LeftBoundary;
        BadNoteRightBoundary = BadNote.GetComponent<BadNote>().RightBoundary;

        CloudSpawnRatio = Cloud.GetComponent<Cloud>().SpawnRatio;
        CloudLeftBoundary = Cloud.GetComponent<Cloud>().LeftBoundary;
        CloudRightBoundary = Cloud.GetComponent<Cloud>().RightBoundary;

        NoteSpawnRatio = Note.GetComponent<Note>().SpawnRatio;
        NoteLeftBoundary = Note.GetComponent<Note>().LeftBoundary;
        NoteRightBoundary = Note.GetComponent<Note>().RightBoundary;

        StartCoroutine(BadNoteSpawn());
        StartCoroutine(CloudSpawn());
        StartCoroutine(NoteSpawn());

        StartCoroutine(UpdatePointsPanel());
    }

    private void Update()
    {
        if (GameOver && GameOverPanel.activeSelf == false)
        {
            GameOverPanel.SetActive(true);
            GameOverPanelText.text = "Points Collected:\n" + Points;
        }
    }

    public static void BadNoteAvoided()
    {
        AddPoints(10);
    }

    public static void NoteCollected(GameObject note)
    {
        Destroy(note);
        AddPoints(50);
    }

    public static void AddPoints(int points)
    {
        Points += points;
    }

    public static void GentlemanIsHit(GameObject badNote)
    {
        if(Lives == 0)
        {
            GameOver = true;
            return;
        }
        Destroy(badNote);
        Lives--;
    }

    private IEnumerator UpdatePointsPanel()
    {
        for (;;)
        {
            PointsLabel.text = Points.ToString();
            LivesLabel.text = Lives.ToString();
            yield return new WaitForSeconds(.1f);
        }
    }

    private IEnumerator BadNoteSpawn()
    {
        for (;;)
        {
            var position = new Vector2(Random.Range(BadNoteLeftBoundary, BadNoteRightBoundary), transform.position.y);
            Instantiate(BadNote, position, Quaternion.identity, EnemyContainer.transform);
            BadNoteSpawnRatio -= 0.001f;
            yield return new WaitForSeconds(BadNoteSpawnRatio);
        }
    }

    private IEnumerator CloudSpawn()
    {
        for (;;)
        {
            var position = new Vector2(Random.Range(CloudLeftBoundary, CloudRightBoundary), transform.position.y);
            Instantiate(Cloud, position, Quaternion.identity, CloudContainer.transform);
            yield return new WaitForSeconds(CloudSpawnRatio);
        }
    }

    private IEnumerator NoteSpawn()
    {
        for (;;)
        {
            var position = new Vector2(Random.Range(NoteLeftBoundary, NoteRightBoundary), transform.position.y);
            Instantiate(Note, position, Quaternion.identity, NoteContainer.transform);
            yield return new WaitForSeconds(NoteSpawnRatio);
        }
    }
}

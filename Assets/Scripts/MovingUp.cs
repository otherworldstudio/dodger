﻿using Assets.Scripts;
using UnityEngine;

public abstract class MovingUp : Base
{
    [Range(0, 5)]
    public float SpawnRatio = 1f;
    
    private void Update()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y + Speed * Time.deltaTime);
        if (transform.position.y > 5)
        {
            Destroy(gameObject);
        }
    }
}